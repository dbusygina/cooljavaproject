import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Candidate> candidates = new ArrayList<>();
        Employer employer = new Employer();
        GetJavaJobCandidate ivan = new GetJavaJobCandidate("Ivan", 22, "man");
        candidates.add(ivan);
        SelfLearnersCandidate anna = new SelfLearnersCandidate("Anna", 25, "woman");
        candidates.add(anna);
        GetJavaJobCandidate petr = new GetJavaJobCandidate("Petr", 33, "man");
        candidates.add(petr);
        SelfLearnersCandidate vasia = new SelfLearnersCandidate("Vasia", 18, "man");
        candidates.add(vasia);
        SelfLearnersCandidate dania = new SelfLearnersCandidate("Dania", 22, "woman");
        candidates.add(dania);
        GetJavaJobCandidate mark = new GetJavaJobCandidate("Mark", 45, "man");
        candidates.add(mark);
        SelfLearnersCandidate maria = new SelfLearnersCandidate("Maria", 29, "woman");
        candidates.add(maria);
        GetJavaJobCandidate olga = new GetJavaJobCandidate("Olga", 28, "woman");
        candidates.add(olga);
        SelfLearnersCandidate anton = new SelfLearnersCandidate("Anton", 31, "woman");
        candidates.add(anton);
        GetJavaJobCandidate alex = new GetJavaJobCandidate("Alex", 23, "man");
        candidates.add(alex);

        for (Candidate candidate : candidates) {
            employer.hello(); // Используем Полиморфизм, в случае с Employer метод hello() будет вести себя характерно для экземпляра этого класса,
            candidate.hello(); // а в случае с Candidate, характерно для экземпляра этого класса, причем по-разному для каждого подкласса
            candidate.describeExperience();
        }
    }
}

abstract class Candidate implements SayHello, DescribeExperience {

    protected String name;  // Используется Абстракция, выбираем нужную информацию(Имя,возвраст,пол, а от остальной абстрагируемся)
    private int age;        // Используется Инкапсуляция с помощью модификаторов private и protected ограничиваем доступ к изменению
    private String sex;

    public int getAge() {  //Используется Инкапсуляция с помощью геттеров-сеттеров
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Candidate(String name, int age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public void hello() {
        System.out.println("hi! my name is " + name + "!");
    }

    public void describeExperience() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class GetJavaJobCandidate extends Candidate { //Используется Наследование
    public GetJavaJobCandidate(String name, int age, String sex) {
        super(name, age, sex);
    }

    @Override
    public void describeExperience() {
        System.out.println("I’ve been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code");
    }
}


class SelfLearnersCandidate extends Candidate { //Используется Наследование
    SelfLearnersCandidate(String name, int age, String sex) {
        super(name, age, sex);
    }


    @Override
    public void describeExperience() {
        System.out.println("I passed successfully getJavaJob exams and code reviews");
    }
}

class Employer implements SayHello {
    @Override
    public void hello() {
        System.out.println("Hi! introduce yourself and describe your java experience please");
    }
}

interface SayHello {
    void hello();
}

interface DescribeExperience {
    void describeExperience();
}

